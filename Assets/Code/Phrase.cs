﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameMode
{
    None,
    Start,
    Cut,
    AnimateRobot,
    End,
}

public class Phrase : MonoBehaviour
{
    public string[] Phrases;
    public float m_letterSize = 1;

    public GameObject m_letterPrefab;
    public GameObject m_cutterPrefab;

    public Transform m_letterScenePosition;

    public UIManager m_uiManager;
    public PositionTweener m_curtain;
    public Robot m_robot;

    public Transform m_letterBackGround;

    private int m_phraseIndex;


    private List<TextMesh> m_letters;
    private List<GameObject> m_cutters;


	void Start() 
    {
        m_letters = new List<TextMesh>();
        m_cutters = new List<GameObject>();
        m_curtain.Up(RobotEnterScene);
        m_letterBackGround.gameObject.SetActive(false);
    }

    private void RobotEnterScene()
    {
        m_robot.PlayEnterScene(ReadyToGo);
    }

    private void ReadyToGo()
    {
        m_curtain.Down(()=>m_uiManager.Mode = GameMode.Start);
    }

    public void OnStartGame()
    {
        m_uiManager.Mode = GameMode.Cut;        
        m_curtain.Down(ChoseRandomText);
    }

    public void OnStartCutting()
    {
        m_uiManager.Mode = GameMode.AnimateRobot;
        m_curtain.Up(RoboCut);
    }

    private void RoboCut()
    {
        StartCoroutine(AnimateCut());
    }

    private IEnumerator AnimateCut()
    {
        Transform trs = m_letters[0].transform;

        List<Cutter> positions = new List<Cutter>(m_cutters.Count);
        string phrase = "";
        while (trs)
        {
            Letter letter = trs.GetComponent<Letter>();
            if (letter)
            {
                phrase += letter.name;
            }
            else
            {
                phrase += " ";
                positions.Add(trs.GetComponent<Cutter>());
            }

            trs = trs.childCount > 0 ? trs.GetChild(0) : null;
        }

        foreach (Cutter cutter in positions)
        {
            bool animating = true;
            Vector3 position = cutter.transform.position;
            position.y = 0;
            m_robot.Goto(position,()=>animating=false);
            while (animating)
            {
                yield return null;
            }
            animating = true;
            m_robot.PlayCutAnimation(()=>animating=false);
            while (animating)
            {
                yield return null;
            }
            cutter.transform.localPosition += new Vector3(m_letterSize, 0, 0);
            m_letterScenePosition.Translate(new Vector3(-m_letterSize/2,0,0),Space.World);
            m_letterBackGround.localScale += new Vector3(m_letterSize,0,0);
            cutter.transform.GetChild(0).parent = cutter.transform.parent;
            cutter.gameObject.SetActive(false);
        }

        if (phrase == Phrases[m_phraseIndex])
        {
            m_robot.PlayWin();
        }
        else
        {
            m_robot.PlayLoose();
        }

        m_uiManager.Mode = GameMode.End;
    }

    private void ChoseRandomText()
    {
        m_phraseIndex = Random.Range(0, Phrases.Length);
        GenerateText(Phrases[m_phraseIndex]);
    }

    private void GenerateText(string phrase)
    {
        foreach (char c in phrase)
        {
            if (c != ' ')
            {
                AddLetter(c);
            }
            else
            {
                AddCutter();
            }
        }
        PositionLetters();
        PositionCutters();
    }

    private void PositionCutters()
    {
        Letter letter = m_letters[0].GetComponent<Letter>();
        foreach (GameObject go in m_cutters)
        {
            Cutter cutter = go.GetComponent<Cutter>();
            cutter.MoveToLetter(letter);
            cutter.transform.Translate(new Vector3(-m_letterSize, 0,0), Space.World);
        }
    }

    private void AddCutter()
    {
        GameObject newCutter = Instantiate(m_cutterPrefab);
        m_cutters.Add(newCutter);
    }

    private void ClearCutters()
    {
        foreach (GameObject cutter in m_cutters)
        {
            Destroy(cutter);
        }
        m_cutters.Clear();
    }

    private void ClearText()
    {
        m_letterBackGround.gameObject.SetActive(false);
        foreach (TextMesh textMesh in m_letters)
        {
            Destroy(textMesh.gameObject);
        }
        m_letters.Clear();
    }

    private void PositionLetters()
    {
        m_letterBackGround.gameObject.SetActive(true);
        Transform root = m_letterScenePosition;

        Vector3 position = root.position;
        float cameraX = Camera.main.transform.position.x;
        position.x = cameraX - (m_letters.Count+2) * m_letterSize / 2;
        root.position = position;

        position = m_letterBackGround.position;
        position.x = cameraX;
        m_letterBackGround.position = position;
        m_letterBackGround.localScale = new Vector3(m_letters.Count * m_letterSize + 0.1f, 1, 1);

        foreach (TextMesh textMesh in m_letters)
        {
            Transform letterTransform = textMesh.transform;
            letterTransform.parent = root;
            letterTransform.localPosition = new Vector3(m_letterSize,0,0);
            root = letterTransform;
        }
    }

    private void AddLetter(char c)
    {
        GameObject newLetter = Instantiate(m_letterPrefab);
        TextMesh textMesh = newLetter.GetComponent<TextMesh>();
        textMesh.name = textMesh.text = c.ToString();
        m_letters.Add(textMesh);
    }

    public void ResetGame()
    {
        m_robot.Reset();
        ClearText();
        ClearCutters();
        m_curtain.Down(()=>m_uiManager.Mode = GameMode.Start);
    }

}