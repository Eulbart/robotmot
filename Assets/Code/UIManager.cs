﻿using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{



    public GameObject m_startRoot;
    public GameObject m_cutRoot;
    public GameObject m_animateRobotRoot;
    public GameObject m_endRoot;


    public GameMode Mode
    {
        get { return m_mode; }
        set
        {
            if (m_mode == value)
                return;

            GameObject old = m_interfaceRoots[m_mode];
            if (old)
            {
                old.SetActive(false);
            }

            m_mode = value;

            GameObject current = m_interfaceRoots[m_mode];
            if (current)
            {
                current.SetActive(true);
            }
        }
    }


    private Dictionary<GameMode,GameObject> m_interfaceRoots;
    private GameMode m_mode;


    void Awake()
    {
        m_interfaceRoots = new Dictionary<GameMode, GameObject>
        {
            {GameMode.Start, m_startRoot},
            {GameMode.Cut, m_cutRoot},
            {GameMode.AnimateRobot, m_animateRobotRoot},
            {GameMode.End, m_endRoot},
        };
        m_mode = GameMode.None;
        foreach (KeyValuePair<GameMode, GameObject> keyValuePair in m_interfaceRoots)
        {
            GameObject uiRoot = keyValuePair.Value;
            if (uiRoot)
            {
                uiRoot.SetActive(false);
            }
        }
        m_interfaceRoots.Add(GameMode.None, null);
    }

}