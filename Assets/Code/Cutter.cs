﻿using UnityEngine;


public class Cutter : MonoBehaviour
{
    public Vector3 m_positionOffset;

    private bool m_dragged;
    private Vector3 m_worldStartPosition;

    private Transform m_previousParent;
    private Transform m_previousChild;
    private Letter m_dropOn;

    void OnMouseDown()
    {
        m_worldStartPosition = transform.position;
        m_dragged = true;
        m_previousParent = transform.parent;
        m_previousChild = transform.childCount==0?null:transform.GetChild(0);
        if (m_previousChild)
        {
            m_previousChild.parent = m_previousParent;
        }
        transform.parent = null;
    }

    void OnMouseUp()
    {
        m_dragged = false;
        if (!MoveToLetter(m_dropOn,true))
        {
            ReturnToOriginalPosition();
        }
    }

    public bool MoveToLetter(Letter dropedOn, bool attach=false)
    {
        if (dropedOn)
        {
            Transform dropedOnTransform = dropedOn.transform;
            Transform root = dropedOnTransform.parent;

            m_dropOn = dropedOn;

            transform.position = dropedOnTransform.position + m_positionOffset;
            if (attach)
            {
                transform.parent = root;
                dropedOnTransform.parent = transform;
            }
            return true;
        }
        return false;
    }

    private void ReturnToOriginalPosition()
    {
        transform.position = m_worldStartPosition;
        transform.parent = m_previousParent;
        if (m_previousChild)
        {
            m_previousChild.parent = transform;
        }
    }


    Letter GetLetterUnderMouse()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hits = Physics.RaycastAll(ray);

        foreach (RaycastHit hit in hits)
        {
            Letter letter = hit.collider.GetComponent<Letter>();
            if (letter)
            {
                // make sure we cut between two letters
                if (letter.transform.parent 
                && letter.transform.parent.GetComponent<Letter>())
                {
                    return letter;
                }
            }
        }
        return null;
    }

    void Update()
    {
        if (!m_dragged)
            return;

        MoveToLetter(GetLetterUnderMouse());
    }
}
