﻿using System;
using System.Collections;
using UnityEngine;

public class PositionTweener : MonoBehaviour
{
    public Vector3 m_downPosition;
    public Vector3 m_upPosition;

    public float m_movementDuration = 0.5f;

    private Vector3 m_targetPosition;
    private Vector3 m_startPosition;
    private float m_timer;

    public bool IsMoving()
    {
        return m_timer > 0;
    }
	
	// Update is called once per frame
	void Update () 
    {
	    if (m_timer > 0)
	    {
	        float t = m_timer/m_movementDuration;
	        m_timer -= Time.deltaTime;
	        transform.position = Vector3.Lerp(m_targetPosition, m_startPosition, t);
	    }
	}

    public void Up(Action whenDone=null)
    {
        StartCoroutine(Goto(m_upPosition, whenDone));
    }

    public void Down(Action whenDone=null)
    {
        StartCoroutine(Goto(m_downPosition, whenDone));
    }

    private IEnumerator Goto(Vector3 position, Action whenDone)
    {
        m_startPosition = transform.position;
        m_targetPosition = position;
        m_timer = m_movementDuration;
        while (IsMoving())
        {
            yield return null;
        }
        transform.position = m_targetPosition;
        if (whenDone!=null)
        {
            whenDone.Invoke();
        }
    }
}
