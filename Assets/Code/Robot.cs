﻿using System;
using UnityEngine;
using System.Collections;

public class Robot : MonoBehaviour
{
    private Vector3 m_startPosition;

    private PositionTweener m_tweener;

    public Transform m_head;
    public Transform m_arm;

    public float m_yesAmplitude = 2;
    public float m_noAmplitude = 4;
    public float m_yesSpeed = 3;
    public float m_noSpeed = 3;

    public float m_cutAngle = 90;

    enum IdleMode
    {
        None,
        Yes,
        No,
        Running
    }

    private IdleMode m_idleMode;

    private float m_timer;

    void Update()
    {
        Vector3 lookAtPosition = m_head.transform.position;

        switch (m_idleMode)
        {
            case IdleMode.None:
                m_timer = 0;
                lookAtPosition.z -= 10;
                break;
            case IdleMode.Yes:
                lookAtPosition.y += m_yesAmplitude * Mathf.Sin(m_timer * m_yesSpeed);
                lookAtPosition.z -= 10;
                m_timer += Time.deltaTime;
                break;
            case IdleMode.No:
                lookAtPosition.x += m_noAmplitude * Mathf.Sin(m_timer * m_noSpeed);
                lookAtPosition.z -= 10;
                m_timer += Time.deltaTime;
                break;
            case IdleMode.Running:
                lookAtPosition += new Vector3(10, 10, 0);
                break;
        }

        Vector3 relativePos = lookAtPosition - m_head.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos);
        m_head.rotation = rotation;
    }


    void Awake()
    {
        m_startPosition = transform.position;
        m_tweener = GetComponent<PositionTweener>();
        m_idleMode = IdleMode.None;
    }

    public void Goto(Vector3 position, Action whenDone=null)
    {
        m_tweener.m_downPosition = transform.position;
        m_tweener.m_downPosition.x = position.x;
        m_tweener.Down(whenDone);
    }

    public void Reset(Action whenDone = null)
    {
        m_idleMode = IdleMode.None;
    }

    public void PlayCutAnimation(Action whenDone = null)
    {
        StartCoroutine(PlayCutAnimationCoroutine(whenDone));
    }

    private IEnumerator PlayCutAnimationCoroutine(Action whenDone)
    {
        const float duration = 0.2f;

        for (float timer = 0; timer < duration; timer += Time.deltaTime)
        {
            m_arm.localEulerAngles = new Vector3(0, Mathf.Lerp(56, 0, timer / duration), 0);
            yield return null;
        }

        for (float timer = 0; timer < duration; timer += Time.deltaTime)
        {
            m_arm.localEulerAngles = new Vector3(Mathf.Lerp(0, m_cutAngle, timer/duration),0,0);
            yield return null;
        }
        for (float timer = 0; timer < duration; timer += Time.deltaTime)
        {
            m_arm.localEulerAngles = new Vector3(Mathf.Lerp(m_cutAngle, 0, timer / duration), 0, 0);
            yield return null;
        }

        for (float timer = 0; timer < duration; timer += Time.deltaTime)
        {
            m_arm.localEulerAngles = new Vector3(0, Mathf.Lerp(0, 56, timer / duration), 0);
            yield return null;
        }


        m_arm.localEulerAngles = new Vector3(0,56,0);

        if (whenDone != null)
        {
            whenDone.Invoke();
        }
    }

    public bool IsMoving()
    {
        return m_tweener.IsMoving();
    }

    public void PlayWin()
    {
        m_idleMode = IdleMode.Yes;
        
    }

    public void PlayLoose()
    {
        m_idleMode = IdleMode.No;
    }

    public void PlayEnterScene(Action whenDone)
    {
        Vector3 position = m_startPosition;
        position.x = Camera.main.transform.position.x;
        m_idleMode = IdleMode.Running;
        Goto(position, () => FinishEnterScene(whenDone));
    }

    private void FinishEnterScene(Action whenDone)
    {
        StartCoroutine(FinishEnterSceneCoroutine(whenDone));
    }

    private IEnumerator FinishEnterSceneCoroutine(Action whenDone)
    {
        transform.rotation = Quaternion.Euler(0, 180, 0);
        m_idleMode = IdleMode.None;
        yield return new WaitForSeconds(1);
        if (whenDone != null)
        {
            whenDone.Invoke();
        }
    }
}
